# TALLER 3 - PROGRAMACIÓN EN LA WEB

Carpeta con el desarrollo del taller 3 de la asigntura de Programación en la Web _(22967)_ dictada por el docente Henry Andrés Jiménez Herrera.

Este taller consistió en la elaboración de una app matemática: una calculadora en donde se escogía una de las 2 funciones: básica o graficadora. 

La idea es acentuar los conceptos básicos, conocer maś a fondo el maquetado _(HTML)_, el manejo de estilos _(CSS)_ y la interacción con el usuario _(Javascript)_.

En el taller se usaron librerías de _Javascript_ para facilitar la labor y que fuese más vistosa para el usuario final.

También se colocó en línea usando GitLab Pages, [aquí](https://hopkeinst.gitlab.io/progweb_20211/Taller3_Calculadora/) se puede ver e interactuar con ella.

---------------

La web fue desarrollada por:

- Jorge Saul Castillo Jaimes - 2111127
**Grupo H1**
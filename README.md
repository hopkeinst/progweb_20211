# TALLERES DE PROGRAMACIÓN EN LA WEB

En este repositorio se estarán subiendo los talleres desarrollados en la asignatura de Programación en la Web _(22967)_ dictada por el docente Henry Andrés Jiménez Herrera.

Todos los talleres están dentro de la carpeta `public` para que puedan ser observados en línea en el enlace [https://hopkeinst.gitlab.io/progweb_20211](https://hopkeinst.gitlab.io/progweb_20211) usando la herramienta de GitLab Pages.

Dentro de los talleres aquí subidos están:

- [Taller 2](public/Taller2): Taller que combina conocimientos de GIT _(trabajo colaborativo)_, HTML y CSS nativo. **Visualización web:** [Taller 2](https://hopkeinst.gitlab.io/progweb_20211/Taller2).

- [Taller 3](public/Taller3_Calculadora): Taller que combina conocimientos de GIT _(trabajo colaborativo)_, HTML, CSS y JS. Consiste en crear una calculadora con 2 opciones principales: calculadora básica y calculadora graficadora.  **Visualización web:** [Taller 3](https://hopkeinst.gitlab.io/progweb_20211/Taller3_Calculadora).


Dudas, comentarios y demás al correo *hopkeinst@gmail.com* .

Hola Nataly, buen día.
